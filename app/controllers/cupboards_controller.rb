class CupboardsController < ApplicationController
  before_action :current_user_active
  before_action :set_cupboard, except: [:new, :create]


  def new
    @cupboard = Cupboard.new
    @cupboards = Cupboard.all
    @user = current_user
    @users = User.all 
  end

  def create
    @cupboard = Cupboard.new(cupboard_params)
      if @cupboard.save
        flash[:notice] = "Cupboard successfuly added."
        redirect_to new_cupboard_path
      else
        render :new
      end
  end

  def show  
    @cupboards = Cupboard.all   
  end

  def edit

  end


  def update
   if @cupboard.update(cupboard_params);
     redirect_to new_cupboard_path, notice: 'Cupboard was successfully updated.'
   else
     render :edit
   end
  end

  def destroy
       @cupboard.destroy
       redirect_to new_cupboard_path, notice: 'Cupboard was successfully destroyed.'
  end


    private

    def cupboard_params
      params.require(:cupboard).permit( :name, :description)
    end 

    def set_cupboard
    @cupboard = Cupboard.find(params[:id])
    end

end

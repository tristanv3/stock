class HomeController < ApplicationController
  before_action :current_user_active


    def show  
      @user = current_user
      @item_users = ItemUser.all
      @items = Item.all
      @cupboards = Cupboard.all
      @users = User.all 
      #.includes(:XXX) preloads requested data 1 query at database not multiple!!
      @active = ItemUser.active?.includes(:item)     
    end
end

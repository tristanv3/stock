class ItemUsersController < ApplicationController
  before_action :current_user_active

 
    def index  
      @item_users = ItemUser.all
      @items = Item.all
      @cupboards = Cupboard.all
      @users = User.all      
    end

  def new
    @item_user = ItemUser.new
    @user = current_user
    @item = Item.find_by_id(params[:id])
    
  end

  def renew
    @current_user = current_user
    @item_user = ItemUser.find_by_id(params[:id])
    ItemUser.renew(params[:id])
    redirect_to show_path
    flash[:notice] = "Renewed for 2 days from now."
  end

  def create  
      @item = Item.find_by_id(params[:item_user][:item_id].to_i)
      @item_user = ItemUser.new(item_user_params)
      #@item_user.assign_attributes(item_id: Item.find_by_id(params[:item_id].to_i))
      @item_user.checkout_date = Date.today
      @item_user.return_date = Date.today + 2.days
      @item_user.status = 1
      @item_user.item_id = @item.id
      @item_user.user_id = current_user.id
      @item.decrement!(:quantity, params[:item_user][:quantity].to_i)
    if @item_user.save

      redirect_to items_path, notice: 'Item booked out.'
    else
      render :new
    end
  end


  def disable
    borrowed_qty = ItemUser.find_by_id(params[:id]).quantity.to_i
    @borrowed_item = ItemUser.find_by_id(params[:id]).item
    @current_user = current_user
    @item_user = ItemUser.find_by_id(params[:id])
    @item_user.status = 0
    ItemUser.disable(params[:id])
    redirect_to show_path
    flash[:notice] = "Item marked as returned. Thank you!"    
  end


  private
    def set_item_user
      @item_user = Item_user.find(params[:id])
    end

    def item_user_params
      params.require(:item_user).permit(:quantity, :status, :checkout_date, :return_date, :item_id, :user_id)
    end

end

class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :current_user_active
  
  def index
    @items = Item.all
    @item = Item.new

  end

  def new
    @item = Item.new
    @cupboards = Cupboard.all
  end

  def edit
    @cupboards = Cupboard.all
    @item = Item.find(params[:id])
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      redirect_to items_path, notice: 'Item created.'
    else
      render :new
    end
  end

  def update
    if @item.update(item_params)
      redirect_to items_path, notice: 'Item updated.'
    else
      render :edit
    end
  end

  def destroy
    @item.destroy
    redirect_to items_url, notice: 'Item destroyed.'
  end



  private
  
  def set_item
    @item = Item.find(params[:id])
  end

  def item_params
    params.require(:item).permit(:cupboard_id, :name, :category, :quantity, :description, :remaining_quantity, :product_code, :asset_type)
  end
end
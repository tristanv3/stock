class SessionsController < ApplicationController
  before_action :current_user_active, except: [:new, :create, :destroy]

  def new
  end

  def show
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticated(params[:password])
      session[:id] = user.id
      redirect_to session_path, notice: "#User:{user.id} logged in!"
    else
      flash[:danger] = 'Invalid email/password combination'
      render 'new'
  end

  def destroy
    if current_user.present?
      session.delete(:id)
      redirect_to login_path
    end
  end
end
end
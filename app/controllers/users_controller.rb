class UsersController < ApplicationController
  before_action :current_user_active, except: [:new, :create, :destroy]
  before_action :set_user, except: [:new, :create]

  
  def new
    @user = User.new
  end

  def create
    @user = User.new(users_params)
      if @user.save
        flash[:notice] = "Registration successful."
        redirect_to show_path
      else
        render :new
      end
  end

  def edit
  end  


  def update
   if @user.update(users_params);
     redirect_to show_path, notice: 'User was successfully updated.'
   else
     render :edit
   end
  end

  def destroy
    if @user.item_users.where(status: 1).count == 0
       @user.destroy
       redirect_to show_path, notice: 'Member was successfully destroyed.'
   else
      flash[:alert] = 'This user has active orders, please check items back in before trying to remove them from the system.'
      redirect_to show_path
    end
  end

  private

    def users_params
      params.require(:user).permit( :name, :email, :password, :user_level, :password_confirmation)
    end 

   def set_user
    @user = User.find(params[:id])
   end


end

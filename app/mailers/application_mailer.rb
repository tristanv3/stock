class ApplicationMailer < ActionMailer::Base
  default from: 'Test@Test.com'
  layout 'mailer'
end

class PasswordResetMailer < ApplicationMailer
  default from: 'tvarley@researchbods.com'
  def reset_email(user)
    @user = user
    mail(to: @user.email, subject: 'Password reset instructions')
  end
end
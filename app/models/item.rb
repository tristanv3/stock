class Item < ApplicationRecord
  has_many :item_users, dependent: :destroy
  has_many :users, through: :item_users
  belongs_to :cupboard


  validates :name, presence: true
  validates :product_code, presence: true
  validates :description, presence: true
  validates :asset_type, presence: true
  validates :quantity, presence: true
end

class ItemUser < ApplicationRecord
  validate :quant_check
  belongs_to :item
  belongs_to :user

  validates :quantity, presence: true


  #Could be done using scopes READ UP
  def self.active?
    ItemUser.where(status: 1)
  end

  def self.inactive?
    ItemUser.where(status: 0)
  end

  def self.expired?
    ItemUser.where("expire_at < ?", Date.today).where(status: 1)
  end

  def self.renew id
    @item_user = ItemUser.where(id: id)
    @item_user.update(return_date: 2.days.from_now)
  end

  def self.disable id
    @item_user = ItemUser.where(id: id)
    @item_user.update(status: 0)
  end



private
  def quant_check
    if quantity > Item.find(item_id).quantity 
        self.errors.add(:quantity, :blank , message: "- Not enough stock")
    end
  end




  
end

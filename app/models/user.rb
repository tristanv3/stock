class User < ApplicationRecord
  attr_accessor :password, :password_confirmation

  has_many :item_users, dependent: :destroy

  validates :name, presence: true
  validates :email, presence: true


 
  acts_as_authentic do |c| 
    c.login_field = :email
     c.ignore_blank_passwords = true
     c.validate_password_field = false
     c.validate_email_field = false
      c.validate_login_field = false
      c.validate_password_field = false
    c.crypto_provider = Authlogic::CryptoProviders::SCrypt
  end
  
  def deliver_password_reset_instructions!
    reset_perishable_token!
    PasswordResetMailer.reset_email(self).deliver_now
  end

  scope :user_type, ->(user_type) { where("user_level = ?", user_type)}

  def is_admin?
    self.user_level == 1
  end


end

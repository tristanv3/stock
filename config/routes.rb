Rails.application.routes.draw do
  
  root to: 'pages#index'

  get '/show', to: 'home#show'

  resources :item_users
  resources :items
  resources :cupboards
  resources :users
  resources :user_sessions, only: [:create, :destroy]
  resources :password_resets, only: [:new, :create, :edit, :update]


  delete '/sign_out', to: 'user_sessions#destroy', as: :sign_out
  get '/sign_in', to: 'user_sessions#new', as: :sign_in
  get '/sign_up', to: 'users#new', as: :sign_up #is this old/needed as xxxx?
  get '/new_cupboard', to: 'cupboards#new'
  put '/edit_cupboard', to: 'cupboards#edit'
 
 

  get 'renew/:id' => 'item_users#renew'
  get 'return/:id' => 'item_users#disable'


 
end

class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name
      t.string :product_code
      t.text :description
      t.integer :asset_type
      t.integer :quantity
      t.string :cupboard_name

      t.references :cupboard

      t.timestamps
    end
  end
end

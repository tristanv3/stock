class CreateLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :logs do |t|
      t.integer :quantity
      t.datetime :checkout_date
      t.datetime :return_date

      t.references :item
      t.references :user

      t.timestamps
    end
  end
end

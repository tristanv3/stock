class RemoveCupboardNameFromItems < ActiveRecord::Migration[5.1]
  def change
    remove_column :items, :cupboard_name, :string
  end
end

class ChangeAssetTypeToBeBinaryFromItems < ActiveRecord::Migration[5.1]
  def change

    reversible do |change|
      change.up { change_column :items, :asset_type, :binary }
      change.down { change_column :items, :asset_type, :integer }
    end
    
  end
end

class RenameTableLog < ActiveRecord::Migration[5.1]
  def change
    rename_table :logs, :item_users
  end
end

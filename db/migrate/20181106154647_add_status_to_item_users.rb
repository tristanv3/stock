class AddStatusToItemUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :item_users, :status, :binary
  end
end
